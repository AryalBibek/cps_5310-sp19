function error = directmethError(p)
  
    %days = 0:12;
    S = [762 740 650 400 250 120 80 50 20 18 15 13 10];
    I = [1 20 80 220 300 260 240 190 120 80 20 5 2];

    [t,y] = ode45(@directmeth, [0:12], [S(1); I(1)],[],p);
  
    dydtNew = (y(:,1)-S').^2 + (y(:,2)-I').^2
  
    error = sum(dydtNew);
  
end

